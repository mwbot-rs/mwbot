## 0.2.2 / 2022-10-01
* Git repository moved to [Wikimedia GitLab](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot).
* Issue tracking moved to [Wikimedia Phabricator](https://phabricator.wikimedia.org/project/profile/6182/).

## 0.2.1 / 2022-09-19
* Properly depend on mwtitle 0.2.0.

## 0.2.0 / 2022-09-19
* Increase MSRV to 1.60.

## 0.2.0-alpha.2 / 2022-01-06
* [BREAKING] Use mwtitle 0.2.0, which is needed to make Error clonable.

## 0.2.0-alpha.1 / 2022-01-04
* [BREAKING] Make Error type cloneable, various upstream errors are now
  wrapped in `Arc<T>`.
* [BREAKING] Rename errors to drop redundant "Error" suffix (fixes [#34](https://gitlab.com/mwbot-rs/mwbot/-/issues/34)):
  * `JsonError` → `InvalidJson`
  * `LockError` → `LockFailure`
  * `TokenError` → `TokenFailure`
  * `PermissionsError` → `PermissionDenied`
  * `MaxlagError` → `Maxlag`
  * `InternalError` → `InternalException`
  * `UnknownError` → `Unknown`
* [BREAKING] Remove deprecated field from `EditConflict`
* [BREAKING] Rename and improve `is_*_error` functions:
  * `is_save_error` → `is_page_related`
  * `is_block_error` → `is_sitewide_block`

## 0.1.7 / 2021-12-24
* New errors:
  * `ContentTooBig`
  * `InvalidPage` (title cannot be a local page)
  * `InvalidTitle` (`From<mwtitle::Error>`)
* Properly generate `EditConflict`. The single field it has is deprecated and
  now always an empty string.
* [`mwtitle`](https://docs.rs/mwtitle) is now a dependency by default. It can
  be disabled by disabling the `from-mwtitle` feature.

## 0.1.6 / 2021-11-15
* New errors:
  * `Blocked` (sitewide block)
  * `PartiallyBlocked`
  * `UnknownBlock` (unable to parse block information)

## 0.1.5 / 2021-11-12
* New errors:
  * `GloballyBlocked`
  * `GloballyRangeBlocked`
  * `GloballyXFFBlocked`
  * `InternalError` (internal MediaWiki exceptions)
  * `Readonly`
  * `UnknownSaveFailure` (for times we don't know the actual error)
* Add `is_block_error()` helper function
* Add `should_retry()` helper function
* Improve output for errors `#[from]` another error

## 0.1.4 / 2021-11-11
* Add separate error type for `{{nobots}}`: `Error:Nobots`.
* Remove fields from `Error:ProtectedPage`, expect caller to already know
  page title.
* Add error type for spam filter: `Error:SpamFilter`.
* [DEPRECATED] The `from-serde_json` feature is a no-op, the dependency is now
  always required.

## 0.1.3 / 2021-11-10
* Add BadToken error
* Add MaxlagError

## 0.1.2 / 2021-11-07
* Require Rust 1.56
* Add error types related to login issues

## 0.1.1 / 2021-10-18
* Fix feature name in default `from` feature

## 0.1.0 / 2021-10-18
* Initial release
