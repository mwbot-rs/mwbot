/*
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use thiserror::Error as ThisError;

/// Error class for config and setup
#[non_exhaustive]
#[derive(ThisError, Debug)]
pub enum ConfigError {
    #[error("Cannot find config directory")]
    CannotFindConfig,
    #[error("I/O error: {0}")]
    IoError(#[from] std::io::Error),
    #[error("Invalid TOML: {0}")]
    InvalidTOML(#[from] toml::de::Error),
    #[error("MediaWiki API error: {0}")]
    APIError(#[from] mwapi_errors::Error),
    #[cfg(unix)]
    #[error("Configuration is readable by other users (mode: {0:o}), make it only readable to you, e.g. `chmod 600 mwbot.toml`")]
    ReadableConfig(u32),
    #[cfg(unix)]
    #[error("Configuration is readable by the world (mode: {0:o}), make it only readable to your group, e.g. `chmod 640 mwbot.toml`")]
    WorldReadableConfig(u32),
}
