## 0.4.3 / 2022-10-14
Note: 0.4.2 was never released because of an accident.

* Add a builder for programmatically creating `Bot` instances.

## 0.4.1 / 2022-10-01
* Git repository moved to [Wikimedia GitLab](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot).
* Issue tracking moved to [Wikimedia Phabricator](https://phabricator.wikimedia.org/project/profile/6182/).

## 0.4.0 / 2022-09-19
* Expose a `Page`'s underlying `mwtitle::Title`.
* Increase MSRV to 1.60.

## 0.4.0-alpha.3 / 2022-05-01
* Check `{{nobots}}` against the page's current wikitext, not what we plan on
  saving. Fixes [#10](https://gitlab.com/mwbot-rs/mwbot/-/issues/10).
* [BREAKING] Use `u64` for all revision ID fields. Fixes [#56](https://gitlab.com/mwbot-rs/mwbot/-/issues/56).
* [BREAKING] Once information about a Page has been fetched, it will always
  return information corresponding to the same revision ID (think immutable).
  To get up-to-date information, create a new Page instance. Notably,
  `Page.save()` will return a new Page instance on success.

## 0.4.0-alpha.2 / 2022-01-06
* Stop re-exporting mwapi_responses crate, it never worked.
* `Page.redirect_target()` now preloads page metadata.
* [BREAKING] Update mwapi_errors to 0.2.0-alpha.2
* [BREAKING] Update mwtitle to 0.2.0-alpha.1

## 0.4.0-alpha.1 / 2021-12-25
* Add `Bot::query_response()` bridge for the `mwapi_responses` crate.
  Fixes [#32](https://gitlab.com/mwbot-rs/mwbot/-/issues/32).
* [BREAKING] Use `tracing` instead of `log`. Fixes [#6](https://gitlab.com/mwbot-rs/mwbot/-/issues/6).
* Add recursive category member page generator.
* [BREAKING] Rename `Bot::get_page()` to `page()`, which now returns
  `Result<Page>` and uses the new `mwtitle` crate for local validation.
* [BREAKING] Rename `Bot::get_html()` to `html()`, which now returns an
  `ImmutableWikicode` to make it easier to write multi-threaded programs.
* [BREAKING] Rename `Bot::get_api()` and `get_parsoid()` to `api()` and
  `parsoid()` respectively.
* [BREAKING] Drop `get_` prefix from most `Page` methods.
* Page information (existence, redirect status) is lazy-loaded and cached.
  Fixes [#17](https://gitlab.com/mwbot-rs/mwbot/-/issues/17).
* Overhaul generators, which now preload basic page information. They are also
  less likely to panic.
* Add `Page::is_category()` and `is_file()`.
* [BREAKING] `EditResponse.newtimestamp` is now `Option<Timestamp>` instead of
  a `Option<String>`.
* [BREAKING] `Bot::query_response()` has been removed, it is now part of the
  `mwapi` crate can be used with `bot.api().query_response(...)`.
* Implement edit conflict detection for wikitext based edits. It should also be
  more robust for Parsoid/HTML based edits.
* [BREAKING] The `parsoid` and `responses` re-exports now export the entire
  crate instead of just the prelude.

## 0.3.3 / 2021-12-16
* Fix fields on EditResponse type

## 0.3.2 / 2021-11-21
* Fix accidental inversion of mark_as_bot option.
* Avoid having a `parsoid::Wikicode` in scope over an await.

## 0.3.1 / 2021-11-19
* Fix panic when dealing with invalid titles. Fixes [#33](https://gitlab.com/mwbot-rs/mwbot/-/issues/33).
* Fix {{nobots}} implementation.

## 0.3.0 / 2021-11-14
* An `oauth2_token` can be used in `mwbot.toml` for authentication. This is
  preferred over a bot password because it doesn't require an explit login
  request. Fixes [#8](https://gitlab.com/mwbot-rs/mwbot/-/issues/8).
* Add `mwbot.toml` option to ignore {{nobots}} named `respect_nobots`.
* Overhaul how pages are saved. Generally it is now:
  `page.save(code, &SaveOptions::summary("edit summary")).await?`. A `String`
  of wikitext can also be used. More options like whether to mark the edit
  as bot or change tags to add can be controlled via the new `SaveOptions`.
  Fixes [#18](https://gitlab.com/mwbot-rs/mwbot/-/issues/18).
  * The prep_saving_ functions have been removed.
* Legacy errors from action=edit will now return Error::UnknownSaveFailure.
  Fixes [#9](https://gitlab.com/mwbot-rs/mwbot/-/issues/9).
* Add generators to get all pages in a category and that use a template.
  Fixes [#22](https://gitlab.com/mwbot-rs/mwbot/-/issues/22).

## 0.3.0-alpha.7 / 2021-11-07
* Reuse `mwapi`'s HTTP client for `parsoid`. Requires 0.6.1 of `parsoid`.
* Add `Page::get_redirect_target()` to get a page's redirect target.
* Check file permissions on `mwbot.toml` are not obviously misconfigured. It
  will error if the file is world readable, and in some cases, group readable.
  Fixes [#5](https://gitlab.com/mwbot-rs/mwbot/-/issues/5).
* Normalize username used in User-agent header.

## 0.3.0-alpha.6 / 2021-11-01
* Require ^0.6 of `parsoid`.

## 0.3.0-alpha.5 / 2021-11-01
* Require `tokio` ^1.8.0 for functionality used in the edit throttle.
* Remove `Page::new()`, `Bot::get_page()` must be used instead.
* Require a minimum of Rust 1.56.

## 0.3.0-alpha.4 / 2021-10-21
* Overhaul how edits work
  * Require edits to be prepared before they can be saved. This allows for
    setting different flags/parameters when saving the edit.
  * Implement an edit throttle, which defaults to 6 edits/minute.
  * Allow setting `mark_as_bot` and `save_delay` in mwbot.toml under an
   `[edit]` section.
* Have Bot implement `Clone` and `Debug`.

## 0.3.0-alpha.3 / 2021-10-18
* Add `Page::save_wikitext()`
* Use new `mwapi_errors` crate for the Error type. Errors from `Bot::from_*`
  functions implement a new `ConfigError`.

## 0.3.0-alpha.2 / 2021-10-14
* Make authentication optional
* Implement specific error types instead of `anyhow::Error`.
* Implement `{{nobots}}` support.

## 0.3.0-alpha.1 / 2021-10-13
* Initial alpha release
